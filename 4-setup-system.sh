#!/usr/bin/env bash

sudo pacman -Syy
sudo pacman -Sy mesa
sudo pacman -Sy xorg-server xorg-apps xorg-xinit xorg-twm xorg-xclock xterm
echo "Please test if xorg works"
echo "Type exit on all the terminals to leave"
read -p "Press [Enter] key to continue..."
startx
sudo pacman -Sy i3-gaps i3blocks i3lock i3status --noconfirm
git clone https://aur.archlinux.org/yay.git /tmp/yay
cd /tmp/yay
makepkg -csi
pacman -Sy ccache
echo "Please uncomment remove the ! in front of ccache on BUILDENV"
echo 'Also change MAKEFLAGS to MAKEFLAGS="-j<cores+1> -l<cores>'
read -p "Press [Enter] key to continue..."
nano /etc/makepkg.conf
