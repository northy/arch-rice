#!/usr/bin/env bash

echo "Please uncomment your locale(s)"
read -p "Press [Enter] key to continue..."
nano /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
export LANG=en_US.UTF-8
read -p "Zone name: " tvar
ln -sf /usr/share/zoneinfo/$tvar > /etc/localtime
hwclock --systohc --utc
echo "KEYMAP=br-abnt2" > /etc/vconsole.conf
read -p "Host name: " tvar
echo $tvar > /etc/hostname
echo "127.0.0.1    localhost
::1          localhost
127.0.1.1    $tvar.localdomain    $tvar" >> /etc/hosts
echo "Please uncomment multilib"
read -p "Press [Enter] key to continue..."
nano /etc/pacman.conf
systemctl enable dhcpcd
pacman -Syy
echo "Set root password:"
passwd
read -p "Username: " tvar
useradd -m -g users -G wheel,audio,video,optical,storage,power -s /bin/bash $tvar
echo "Set user's password: "
passwd $tvar
echo "Uncomment: %wheel ALL=(ALL) ALL"
echo "Add line: Defaults rootpw"
read -p "Press [Enter] key to continue..."
EDITOR=nano visudo
pacman -Sy bash-completion --noconfirm
mount -t efivarfs efivarfs /sys/firmware/efi/efivars
pacman -Sy grub efibootmgr
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg
pacman -Sy linux-zen-headers
pacman -Sy intel-ucode
