#!/usr/bin/env bash

timedatectl set-ntp true
sudo pacman -Sy
sudo pacman -S pacman-contrib --noconfirm
curl 'https://www.archlinux.org/mirrorlist/?&protocol=http&protocol=https&ip_version=4' > /etc/pacman.d/mirrorlist.backup
sed -i 's/^#Server/Server/' /etc/pacman.d/mirrorlist.backup
rankmirrors -n 6 /etc/pacman.d/mirrorlist.backup > /etc/pacman.d/mirrorlist
pacstrap -i /mnt base base-devel linux-zen nano dhcpcd
genfstab -U -p /mnt >> /mnt/etc/fstab
cp . /mnt/arch-rice/ -r
echo "Entering chroot to setup installed system"
read -p "Press [Enter] To continue..."
arch-chroot /mnt ./arch-rice/3-chroot-configs.bash
umount -R /mnt
reboot
